import os
import numpy as np
import xml.etree.ElementTree as ET
from ase import Atoms
from ase.units import Bohr, Ry, Hartree
from pymatgen.core.lattice import Lattice
from pymatgen.core.structure import Structure
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer

class pwscf_xml(object):
    """
    A class to analyse Quantum Espresso (QE) pwscf xml output files. 

    Takes the pwscf xml data filename and creates a pwscf_xml object which is used by other methods in this class.

    Lengths are in bohr units and energies in hartree energy units.

    Parameters
    ----------
    entry_name: str
        xml filename e.g. 'home/user/QE/salts/NaCl/pwscf.xml' or 'pwscf.xml'
    """

    def __init__(self, filename) -> None:
        self.filename = filename
        self.root = ET.parse(filename).getroot()

    def get_convergence_info(self, print_info=None) -> bool:
        """
        Returns True if scf and geometry optimisation criteria were achieved.

        Parameters
        ----------
        print_info: bool
            If True, prints out scf and opt cycle convergence info.
        """
        if self.root.find('.output/convergence_info/scf_conv/convergence_achieved').text == 'true':
            scf_conv = True
        else:
            scf_conv = False

        if self.root.find('.input/control_variables/calculation').text in {'relax', 'vc-relax'}:
            if self.root.find('.output/convergence_info/opt_conv/convergence_achieved').text == 'true':
                opt_conv = True
            else:
                opt_conv = False
        else:
            opt_conv = None

        if print_info is True:
            if scf_conv:
                print('scf convergence has been achieved in ' + self.root.find(
                    '.output/convergence_info/scf_conv/n_scf_steps').text + ' scf steps')
            else:
                print('scf convergence was not achieved in ' + self.root.find(
                    '.output/convergence_info/scf_conv/n_scf_steps').text + ' scf steps')

            if opt_conv is not None:
                if opt_conv:
                    print('convergence has been achieved in ' + self.root.find(
                        '.output/convergence_info/opt_conv/n_opt_steps').text + ' iterations')
                else:
                    print('convergence was not achieved in ' + self.root.find(
                        '.output/convergence_info/opt_conv/n_opt_steps').text + ' iterations')

        if opt_conv is not None:
            return scf_conv and opt_conv
        else:
            return scf_conv

    @property
    def etot(self) -> float:
        """
        Returns total energy (Ha).
        """
        return float(self.root.find('.output/total_energy/etot').text)

    def get_total_energy(self) -> float:
        """
        Returns total energy (Ha).
        """
        return self.etot

    @property
    def nat(self) -> int:
        """
        Returns number of atoms in cell.
        """
        return int(self.root.find('.output/atomic_structure').attrib['nat'])

    @property
    def nelec(self) -> int:
        """
        Returns number of electrons.
        """
        return int(float(self.root.find('.output/band_structure/nelec').text))

    @property
    def nbnd(self) -> int:
        """
        Returns number of bands.
        """
        #return int(self.root.find('.output/band_structure/nbnd').text)
        return int(self.root.find('.input/bands/nbnd').text)

    def get_lattice(self) -> 'Lattice':
        """
        Returns Pymatgen Lattice object. 

        Example:

        lattice_angles = pwscf_xml('pwscf.xml).get_lattice().angles

        lattice_vectors = pwscf_xml('pwscf.xml).get_lattice().matrix
        """
        cell = self.root.find('.output/atomic_structure/cell')
        cell_matrix = list()
        for cell_vector in cell:
            cell_matrix.append(list(map(float, cell_vector.text.split())))
        return Lattice(cell_matrix)

    def get_initial_lattice(self) -> 'Lattice':
        """
        Returns Pymatgen Lattice object of initial cell. 

        Example:

        lattice_angles = pwscf_xml('pwscf.xml).get_initial_lattice().angles

        lattice_vectors = pwscf_xml('pwscf.xml).get_initial_lattice().matrix
        """
        cell = self.root.find('.input/atomic_structure/cell')
        cell_matrix = list()
        for cell_vector in cell:
            cell_matrix.append(list(map(float, cell_vector.text.split())))
        return Lattice(cell_matrix)

    def get_spacegroup(self) -> 'SpacegroupAnalyzer':
        """
        Returns SpacegroupAnalyzer object.

        Example:

        spacegroup_number = pwscf_xml('pwscf.xml).get_space_group_number()

        spacegroup_symbol = pwscf_xml('pwscf.xml).get_space_group_symbol()
        """
        return SpacegroupAnalyzer(self.get_pymatgen_structure())

    def get_elements(self) -> np.ndarray:
        """
        Returns a numpy array of element names i.e. ['C', 'C', ..., 'C'].
        """
        positions = self.root.find('.output/atomic_structure/atomic_positions')
        elements = list()
        for element in positions:
            elements.append(element.attrib['name'])
        return np.array(elements)

    def get_positions(self) -> np.ndarray:
        """
        Returns a numpy array of cartesian atom positions (Bohr units).
        """
        positions = self.root.find('.output/atomic_structure/atomic_positions')
        position_vectors = list()
        for vector in positions:
            position_vectors.append(list(map(float, vector.text.split())))
        return np.array(position_vectors)

    def get_scaled_positions(self) -> np.ndarray:
        """
        Returns a numpy array of fractional atom coordinates.
        """
        cell_inv = np.linalg.inv(self.get_lattice().matrix) # inverse of cell matrix
        return np.matmul(self.get_positions(), cell_inv)

    def get_forces(self) -> np.ndarray:
        """
        Returns a numpy array of force components on each atom.

        Forces in Ha/Bohr units.

        To convert to eV/Angstom multiply this array by (Hartree / Bohr).
        """
        if self.root.find('.input/control_variables/forces').text == 'true':
            dims = np.array(list(map(int, self.root.find('.output/forces').attrib['dims'].split())))
            dim_x, dim_y = dims[0], dims[1]
            return np.array(list(map(float, self.root.find('.output/forces').text.split()))).reshape(dim_y, dim_x)
        else:
            raise ValueError(
                'Forces were not being calculated in this calculation.')

    def get_stress(self) -> np.ndarray:
        """
        Returns a numpy array of stress components.

        In Ha/Bohr^{3} units.
        """
        if self.root.find('.input/control_variables/stress').text == 'true':
            dims = np.array(list(map(int, self.root.find('.output/stress').attrib['dims'].split())))
            dim_x, dim_y = dims[0], dims[1]
            return np.array(list(map(float, self.root.find('.output/stress').text.split()))).reshape(dim_y, dim_x)
        else:
            raise ValueError(
                'Stress was not being calculated in this calculation.')

    def get_total_stress(self, kbar=None) -> np.ndarray:
        """
        Returns total stress in Ha/Bohr^{3} units. 
        
        Parameters
        ----------
        kbar: bool
            If True, returns in kbar units.
        """
        if kbar == True:
            return (self.get_stress().trace() / 3) * 10 * 29421.02648438959
        else:
            return (self.get_stress().trace() / 3)

    def get_total_drift(self) -> np.ndarray:
        """
        Returns a numpy array of total drift.

        Forces in hartree/bohr units.

        To convert to eV/Angstom multiply this array by (Hartree / Bohr).
        """
        forces = self.get_forces()
        tot_fx, tot_fy, tot_fz = 0, 0, 0
        for force in forces:
            tot_fx += force[0]
            tot_fy += force[1]
            tot_fz += force[2]
        return np.array([tot_fx, tot_fy, tot_fz])
    
    def get_kpt_coords(self) -> np.ndarray:
        """
        Returns a numpy array of k-point coordinates.
        """
        coords = list()
        for kpt in self.root.find('.output/band_structure').findall('ks_energies'):
            coords.append(list(map(float, kpt.find('k_point').text.split())))
        return np.array(coords)

    def get_kpt_weights(self) -> np.ndarray:
        """
        Returns a numpy array of k-point weights.
        """
        weights = list()
        for kpt in self.root.find('.output/band_structure').findall('ks_energies'):
            weights.append(float(kpt.find('k_point').attrib['weight']))
        return np.array(weights)

    def get_occupations(self) -> np.ndarray:
        """
        Returns numpy array with Kohn-Sham orbital occupations for each k-point.

        Example:

        for occupations in pwscf_xml('pwscf.xml).get_occupations():
            print(occupations)
        """
        occupations = list()
        for ks in self.root.find('.output/band_structure').findall('ks_energies'):
            occupations.append(np.array(list(map(float, ks.find('occupations').text.split()))))
        return np.array(occupations)

    def get_evals(self) -> np.ndarray:
        """
        Returns numpy array of Kohn-Sham orbital levels (Ha) for each k-point.

        Example:

        for evals in pwscf_xml('pwscf.xml).get_evals():
            print(evals)
        """
        evals = list()
        for ks in self.root.find('.output/band_structure').findall('ks_energies'):
            evals.append(np.array(list(map(float, ks.find('eigenvalues').text.split()))))
        return np.array(evals)

    @property
    def VBM(self) -> float:
        """
        Returns the value (Ha) of valence band maximum.
        """
        if self.occupations_kind in {'smearing', 'tetrahedra', 'tetrahedra_lin', 'tetrahedra_opt'}:
            VBM = list()
            for k, occupancies in enumerate(self.get_occupations()):
                for i, occupancy in enumerate(occupancies):
                    if occupancy < 0.5:
                        VBM.append(self.get_evals()[k][i-1])
                        break
            return max(VBM)

        elif self.occupations_kind in {'fixed', 'from_input'}:
            return float(self.root.find('.output/band_structure/highestOccupiedLevel').text)
            #if self.root.find('.output/band_structure/highestOccupiedLevel'):
            #    return float(self.root.find('.output/band_structure/highestOccupiedLevel').text)
            #else:
            #    raise ValueError('Error while finding highest occupied level. Consult with maintainer to implement.')

        # elif self.root.find('.output/band_structure/lsda').text == 'true': # spin polarised
        #     nbnd_up = int(self.root.find('.output/band_structure/nbnd_up').text)
        #     nbnd_dw = int(self.root.find('.output/band_structure/nbnd_dw').text)

    @property
    def CBM(self) -> float:
        """
        Returns the value (Ha) of conductance band minimum.
        """
        if self.occupations_kind == 'smearing':
            CBM = list()
            for k, occupancies in enumerate(self.get_occupations()):
                for i, occupancy in enumerate(occupancies):
                    if occupancy < 0.5:
                        CBM.append(self.get_evals()[k][i])
                        break
            return min(CBM)

        elif self.occupations_kind in {'fixed', 'from_input'}:
            return float(self.root.find('.output/band_structure/lowestUnoccupiedLevel').text)
            #if self.root.find('.output/band_structure/lowestUnoccupiedLevel'):
            #    return float(self.root.find('.output/band_structure/lowestUnoccupiedLevel').text)
            #else:
            #    raise ValueError('Error while finding lowest unoccupied level. Consult with maintainer to implement.')

    @property
    def bandgap(self) -> float:
        """
        Returns the bandgap (Ha) value.
        """
        return self.CBM - self.VBM

    @property
    def bandgap_type(self) -> str:
        """
        Returns wether the bandgap type is direct or indirect.
        """
        VBM, CBM = list(), list()
        for k, occupancies in enumerate(self.get_occupations()):
            for i, occupancy in enumerate(occupancies):
                if occupancy < 0.5:
                    VBM.append(self.get_evals()[k][i-1])
                    CBM.append(self.get_evals()[k][i])
                    break
        
        if abs(VBM.index(max(VBM)) - CBM.index(min(CBM))) == 0:
            return 'direct'
        elif abs(VBM.index(max(VBM)) - CBM.index(min(CBM))) > 0:
            return 'indirect'

    @property
    def occupations_kind(self) -> str:
        """
        Returns the kind of Kohn-Sham orbital occupations.
        """
        return self.root.find('.output/band_structure/occupations_kind').text

    def get_pymatgen_structure(self) -> 'Structure':
        """
        Returns a pymatgen structure object.
        """
        return Structure(lattice=self.get_lattice(), species=self.get_elements(), coords=self.get_positions())

    def get_ase_structure(self, Angstrom=None) -> 'Atoms':
        """
        Returns ase Atoms object.

        Parameters
        ----------
        Angstrom: bool
            If True, converts all lengths to Angstroms which can then be viewed by ase.vizualise.view module.
        """
        if Angstrom == True:
            return Atoms(symbols=self.get_elements(), positions=self.get_positions()*Bohr, cell=self.get_lattice().matrix*Bohr)
        else:
            return Atoms(symbols=self.get_elements(), positions=self.get_positions(), cell=self.get_lattice().matrix)

    def get_pymatgen_structure_list(self) -> list:
        """
        Returns a list of pymatgen Structure objects.

        You can access the first and last optimisation structure by indexing this list.
        """
        struct_list = list()
        if self.root.find('.input/control_variables/calculation').text == 'scf':
            return struct_list.append(self.get_pymatgen_structure())
        else:
            for step in self.root.findall('step'):
                cell_matrix, elements, coords = list(), list(), list()
                
                cell = step.find('atomic_structure/cell')
                for cell_vector in cell:
                    cell_matrix.append(list(map(float, cell_vector.text.split())))
                
                positions = step.find('atomic_structure/atomic_positions')
                for element in positions:
                    elements.append(element.attrib['name'])
                    coords.append(list(map(float, element.text.split())))

                struct_list.append(Structure(lattice=Lattice(cell_matrix), species=np.array(
                    elements), coords=np.array(coords)))
            return struct_list

    def get_ase_structure_list(self, Angstrom=None) -> list:
        """
        Returns a list of ase Atoms objects.

        You can access the first and last optimisation structure by indexing this list.

        Parameters
        ----------
        Angstrom: bool
            If True, converts all lengths to Angstroms which can then be viewed by ase.vizualise.view module.
        """
        struct_list = list()
        
        if self.root.find('.input/control_variables/calculation').text == 'scf':
            if Angstrom == True:
                return struct_list.append(self.get_ase_structure(Angstrom=True))
            else:
                return struct_list.append(self.get_ase_structure())

        else:
            for step in self.root.findall('step'):
                cell_matrix, elements, coords = list(), list(), list()

                cell = step.find('atomic_structure/cell')
                for cell_vector in cell:
                    cell_matrix.append(list(map(float, cell_vector.text.split())))
                
                positions = step.find('atomic_structure/atomic_positions')
                for element in positions:
                    elements.append(element.attrib['name'])
                    coords.append(list(map(float, element.text.split())))
            
                if Angstrom == True:
                    struct_list.append(Atoms(symbols=np.array(elements), positions=np.array(coords)*Bohr, cell=np.array(cell_matrix)*Bohr))
                else:
                    struct_list.append(Atoms(symbols=np.array(elements), positions=np.array(coords), cell=np.array(cell_matrix)))
            return struct_list

    @property
    def ecutwfc(self) -> float:
        """
        Returns kinetic energy cutoff (Ha) for wavefunctions.
        """
        return float(self.root.find('.output/basis_set/ecutwfc').text)

    @property
    def ecutrho(self) -> float:
        """
        Returns kinetic energy cutoff (Ha) for charge density and potential.
        """
        return float(self.root.find('.output/basis_set/ecutrho').text)

    @property
    def functional(self) -> str:
        """
        Returns type of functional used.
        """
        return self.root.find('output/dft/functional').text

    @property
    def kpts(self) -> np.ndarray:
        """
        Returns first Brillouin zone sampling points.
        """
        if self.root.find('.output/basis_set/gamma_only').text == 'true':
            return 'gamma'
        elif self.root.find('.output/band_structure/starting_k_points/monkhorst_pack').text == 'Monkhorst-Pack':
            nk1 = int(self.root.find(
                '.output/band_structure/starting_k_points/monkhorst_pack').attrib['nk1'])
            nk2 = int(self.root.find(
                '.output/band_structure/starting_k_points/monkhorst_pack').attrib['nk2'])
            nk3 = int(self.root.find(
                '.output/band_structure/starting_k_points/monkhorst_pack').attrib['nk3'])
            sk1 = int(self.root.find(
                '.output/band_structure/starting_k_points/monkhorst_pack').attrib['k1'])
            sk2 = int(self.root.find(
                '.output/band_structure/starting_k_points/monkhorst_pack').attrib['k2'])
            sk3 = int(self.root.find(
                '.output/band_structure/starting_k_points/monkhorst_pack').attrib['k3'])
            return np.array([[nk1, nk2, nk3], [sk1, sk2, sk3]])
        else:
            raise ValueError(
                'kpts were not gamma point or automatically generated Monkhorst-Pack grid.')

    @property
    def fermi_energy(self) -> float:
        """
        Returns Fermi energy.
        """
        if self.root.find('.input/spin/lsda').text == 'true':
            fermi_energies = list()
            for energy in list(map(float, self.root.find('./output/band_structure/two_fermi_energies').text.split())):
                fermi_energies.append(energy)
            return np.array(fermi_energies)
        else:
            return float(self.root.find('.output/band_structure/fermi_energy').text)

    @property
    def cpu_time(self) -> float:
        """
        Returns total CPU time spent in seconds. 
        """
        return float(self.root.find('.timing_info/total/cpu').text)

    @property
    def ncpus(self) -> int:
        """
        Returns the number of CPUs used in calculation.
        """
        return int(self.root.find('.parallel_info/nprocs').text)

    @property
    def calc_type(self) -> str:
        """
        Returns type of calculation which was performed. 
        """
        return self.root.find('.input/control_variables/calculation').text

    @property
    def tot_magnetization(self) -> float:
        """
        Returns total magnetization.

        M_{Tot} = \int_{cell} (n_{up} - n_{dw}) d^{3} r 

        Total majority spin charge - minority spin charge.
        """
        return float(self.root.find('.output/magnetization/total').text)

    @property
    def abs_magnetization(self) -> float:
        """
        Returns absolute magnetization.

        M_{Abs} = \int_{cell} |n_{up} - n_{dw}| d^{3} r
        """
        return float(self.root.find('.output/magnetization/absolute').text)
